#!/bin/bash

sudo dnf install -y nano

sudo dnf install -y openssh-server

sudo curl -o setup-repos.sh https://raw.githubusercontent.com/webmin/webmin/master/setup-repos.sh
sudo sh setup-repos.sh

sudo dnf install -y webmin

sudo dnf -y install httpd
sudo firewall-cmd --add-service=http
sudo firewall-cmd --runtime-to-permanent

sudo dnf -y install mod_ssl

sudo dnf -y install mariadb-server

sudo dnf update -y
