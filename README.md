# Instruções de linha de comando
Você também enviar arquivos existentes do seu computador usando as instruções abaixo.

## Configuração global do Git
git config --global user.name "Francisco Canzi Bigal"
git config --global user.email "canzibigal1978@gmail.com"
## Criar um novo repositório
git clone git@gitlab.com:bigalinux/fedoraserver.git
cd fedoraserver
git switch --create main
touch README.md
git add README.md
git commit -m "add README"
git push --set-upstream origin main
## Faça push de uma pasta existente
cd existing_folder
git init --initial-branch=main
git remote add origin git@gitlab.com:bigalinux/fedoraserver.git
git add .
git commit -m "Initial commit"
git push --set-upstream origin main
## Faça push de um repositório Git existente
cd existing_repo
git remote rename origin old-origin
git remote add origin git@gitlab.com:bigalinux/fedoraserver.git
git push --set-upstream origin --all
git push --set-upstream origin --tags

## Programas que são instalados
- Nano
- openssh-server
- webmin
- httpd
- mod_ssl
- mariadb-server
- update de sistema
